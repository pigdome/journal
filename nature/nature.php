<?php 

# Get parameter
$param = get_param($argv);

# Set file name
$query_filename = $param['qf'] or die("This script required file name( eg. /path/to/filename )!"); 

# Read query from file
$q_file = fopen("$query_filename", "r") or die("Unable to open file!");
# Set header of xml
$output = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\t<record>\n";

while( $query = fgets($q_file) ) 
{
  $query = trim($query);
  $output .= fetch_json_from_curl($query);
}
fclose($q_file);

# Set footer of xml
$output .= "\n\t</record>";
# print to file
$xml_file = fopen($param['of'], "w") or die("Unable to open file!");
fwrite($xml_file, $output);
fclose($xml_file);












#------------ FUNCTION ----------------------------------------

function fetch_json_from_curl($query)
{
    $output = "";
    $request = "http://www.nature.com/opensearch/request?query=$query&httpAccept=application/json";
    # Use curl to get data 
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $request);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    $data = curl_exec($ch);
    curl_close($ch);

    # Fetch json data
    $data = json_decode($data, true);
    $entry = $data['feed']['entry'];


    for ($i=0; $i < sizeof($entry) ; $i++) 
    { 
        # HEADER
        $output .=  "\t\t<data>\n";
        # Get title
        $output .=  "\t\t\t<title>" . $entry[$i]['title'] . "</title>\n";
        # Get link
        $output .=  "\t\t\t<link>" . $entry[$i]['link'] . "</link>\n";
        # Get creator
        $creator_arr = $entry[$i]['sru:recordData']['pam:message']['pam:article']['xhtml:head']['dc:creator'];
        $creator = "";
        for ($v=0; $v < sizeof($creator_arr); $v++) 
        { 
            if( $v > 0 )
            {
                $creator .= " , " . $creator_arr[$v];   
            }
            else
            {
                $creator .= $creator_arr[$v];      
            }
        }
        $output .=  "\t\t\t<creator>" . $creator . "</creator>\n";        
        # Get issn
        $issn = $entry[$i]['sru:recordData']['pam:message']['pam:article']['xhtml:head']['prism:issn'];
        $output .=  "\t\t\t<issn>" . $issn . "</issn>\n";
        # Get publicationDate
        $publicationDate = $entry[$i]['sru:recordData']['pam:message']['pam:article']['xhtml:head']['prism:publicationDate'];
        $output .=  "\t\t\t<publicationDate>" . $publicationDate . "</publicationDate>\n";
        # Get volume
        $volume = $entry[$i]['sru:recordData']['pam:message']['pam:article']['xhtml:head']['prism:volume'];
        $output .=  "\t\t\t<volume>" . $volume . "</volume>\n";
        # Get number
        $number = $entry[$i]['sru:recordData']['pam:message']['pam:article']['xhtml:head']['prism:number'];
        $output .=  "\t\t\t<number>" . $number . "</number>\n";
        # Get startingPage
        $startingPage = $entry[$i]['sru:recordData']['pam:message']['pam:article']['xhtml:head']['prism:startingPage'];
        $output .=  "\t\t\t<startingPage>" . $startingPage . "</startingPage>\n";
        # Get endingPage
        $endingPage = $entry[$i]['sru:recordData']['pam:message']['pam:article']['xhtml:head']['prism:endingPage'];
        $output .=  "\t\t\t<endingPage>" . $endingPage . "</endingPage>\n";
        # Footer
        $output .=  "\t\t</data>\n";
    }

    return $output;
}

function get_param($argv)
{
    $query_filename = "";
    $output_filename = "";
    for ($i=0; $i < sizeof($argv); $i++) 
    { 
        switch ($argv[$i]) 
        {
            case "-h":help();break;
            case "-f":$query_filename = $argv[$i+1];break;
            case "-o":$output_filename = $argv[$i+1];break;
            default:break;
        }  
    }

    # Set defualt output
    if( $output_filename == "" ) $output_filename = "/tmp/output.xml";

    return array('qf' => $query_filename, 'of' => $output_filename);
}

function help()
{
    print "Usage php test.php -f path/to/file [-o /path/to/output.xml] \n\n";
    die;
}

?>