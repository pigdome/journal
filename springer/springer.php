<?php 

$api_key = "3457b241bb8ff02626b56d2487df402e";
# Get parameter
$param = get_param($argv);

# Set file name
$query_filename = $param['qf'] or die("This script required file name( eg. /path/to/filename )!"); 

# Read query from file
$q_file = fopen("$query_filename", "r") or die("Unable to open file!");

$myfile = fopen("/tmp/output.xml", "w") or die("Unable to open file!");
$doc = new DOMDocument('1.0');
$doc->formatOutput = true;

while( $query = fgets($q_file) ) 
{
  $query = trim($query);
  $query = str_replace(" ","%20",$query);
  fetch_json_from_curl($query);
}
fwrite($myfile, $doc->saveXML() );
fclose($q_file);
fclose($myfile);




#------------ FUNCTION ----------------------------------------

function fetch_json_from_curl($query)
{
    $api_key = $GLOBALS['api_key'];
    $request = "http://api.springer.com/metadata/json?q=name:$query&api_key=$api_key";
    
    $data = get_json($request);
    $records = $data['records'];


    for ($i=0; $i < sizeof($records) ; $i++) 
    { 
        $identifier = $records[$i]['identifier'];
        $details = get_detail_from_id($identifier);
        AddJournal_to_XML($details,$GLOBALS['myfile'],$GLOBALS['doc']);
    }
}

function get_detail_from_id($id)
{
    $api_key = $GLOBALS['api_key'];
    $request = "http://api.springer.com/metadata/json?q=$id&api_key=$api_key";
    $data = get_json($request);

    $details = array();
    $records = $data['records'][0];
    
    
    $details['identifier']      = $records['identifier'];
    $details['title']           = $records['title'];
    $details['author']          = get_value_from_array( $records['creators'] , 'creator' );
    $details['abstract']        = $records['abstract'];
    $details['publisher']       = $records['publisher'];
    $details['keyword']         = get_value_from_array( $data['facets'][5]['values'] , 'value' );
    $details['publicationName'] = $records['publicationName'];
    $details['publicationDate'] = $records['publicationDate']; 
    $details['isbn']            = $records['isbn'];
    $details['volume']          = $records['volume'];
    $details['number']          = $records['number'];
    $details['startingPage']    = $records['startingPage'];
    $details['genre']           = $records['genre'];
    $details['type']            = get_value_from_array( $data['facets'][4]['values'], 'value' );

    return $details;
}

function get_value_from_array($arr_hash,$key)
{
    $output = "";
    for ($i=0; $i < sizeof($arr_hash) ; $i++) 
    { 
        if ( $output == "" ) 
        {
            $output = $arr_hash[$i][$key];  
        }
        else
        {
            $output .=  " | " . $arr_hash[$i][$key];
        }
    }
    return $output;
}



function get_json($request)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $request);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    $data = curl_exec($ch);
    curl_close($ch);

    
    $data = json_decode($data, true);
    return $data;
}

function get_param($argv)
{
    $query_filename = "";
    $output_filename = "";
    for ($i=0; $i < sizeof($argv); $i++) 
    { 
        switch ($argv[$i]) 
        {
            case "-h":help();break;
            case "-f":$query_filename = $argv[$i+1];break;
            case "-o":$output_filename = $argv[$i+1];break;
            default:break;
        }  
    }

    # Set defualt output
    if( $output_filename == "" ) $output_filename = "/tmp/output.xml";

    return array('qf' => $query_filename, 'of' => $output_filename);
}

function AddJournal_to_XML($hash,$myfile,$doc)
{
    $root = $doc->createElement('record');
    $root = $doc->appendChild($root);

    
    foreach ($hash as $key => $value)
    {
        $title = $doc->createElement($key);
        $title = $root->appendChild($title);

        $text = $doc->createTextNode($value);
        $text = $title->appendChild($text);
    }
}

function help()
{
    print "Usage php test.php -f path/to/file [-o /path/to/output.xml] \n\n";
    die;
}

?>