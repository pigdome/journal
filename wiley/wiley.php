<?php 

include './../lib/context.php';


# Get parameter
$param = get_param($argv);

# Set file name
$query_filename = $param['qf'] or die("This script required file name( eg. /path/to/filename )!"); 

# Read query from file
$q_file = fopen("$query_filename", "r") or die("Unable to open file!");

while( $query = fgets($q_file) ) 
{
  $query = trim($query);
  $query = str_replace(" ","+",$query);
  $data  = get_data($query);
}
fclose($q_file);





#------------ FUNCTION ----------------------------------------

function get_data($query)
{
    $hostname = "http://www.degruyter.com";
    $request = "/search?f_0=author&q_0=$query&searchTitles=false&type=journals&type_0=journals";
    
    $data = get_html($hostname . $request);

    $doc = new DOMDocument();
    $doc->loadHTML($data);

    $xpath = new DOMXpath($doc);

    $link_to_full_item = $xpath->query('//h2[@class="itemTitle"]/a');
    for ($i=0; $i < $link_to_full_item->length ; $i++)
    {
        $data = get_html( $hostname . $link_to_full_item->item($i)->getAttribute('href') );
        fetch_full_item($data);
    }
    return "";
}

function fetch_full_item($data)
{
    # Fetch full item
    $doc = new DOMDocument();
    $doc->loadHTML($data);

    $xpath = new DOMXpath($doc);

    $journal_title = $xpath->query('//h1[@id="mainTitle"]')->item(0)->textContent;
    $article_title = $xpath->query('//h1[@class="entryTitle"]')->item(0)->textContent;
    $authors = $xpath->query('//div[@class="contributors"]')->item(0)->textContent;
    $authors = str_replace(" / "," | ",$authors);
    $doi = "doi:" . $xpath->query('//a[@itemprop="citation_doi"]')->item(0)->textContent;
    
    $keywords = "";
    $dom_keywords = $xpath->query('//p[@class="articleBody_keywords"]/a');
    
    for ($i=0; $i < $dom_keywords->length ; $i++) 
    { 
        if( $keywords == "" )
        {
            $keywords .= $dom_keywords->item($i)->textContent;
        }
        else
        {
            $keywords .= " | " . $dom_keywords->item($i)->textContent;
        }
    }
    $abstract = $xpath->query('//div[@class="articleBody_abstract"]/p')->item(0)->textContent;
    
    print $journal_title ."\n";
    print $article_title ."\n";
    print $authors ."\n";
    print $doi ."\n";
    print $keywords ."\n";
    print $abstract ."\n";
}

function get_html($request)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $request);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

function get_param($argv)
{
    $query_filename = "";
    $output_filename = "";
    for ($i=0; $i < sizeof($argv); $i++) 
    { 
        switch ($argv[$i]) 
        {
            case "-h":help();break;
            case "-f":$query_filename = $argv[$i+1];break;
            case "-o":$output_filename = $argv[$i+1];break;
            default:break;
        }  
    }

    # Set defualt output
    if( $output_filename == "" ) $output_filename = "/tmp/output.xml";

    return array('qf' => $query_filename, 'of' => $output_filename);
}

function help()
{
    print "Usage php test.php -f path/to/file [-o /path/to/output.xml] \n\n";
    die;
}

?>