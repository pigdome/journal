<?php 

include './../lib/context.php';


# Get parameter
$param = get_param($argv);

# Set file name
$query_filename = $param['qf'] or die("This script required file name( eg. /path/to/filename )!"); 

# Read query from file
$q_file = fopen("$query_filename", "r") or die("Unable to open file!");

$myfile = fopen("/tmp/output.xml", "w") or die("Unable to open file!");
$doc = new DOMDocument('1.0');
$doc->formatOutput = true;

while( $query = fgets($q_file) ) 
{
  $query = trim($query);
  $query = str_replace(" ","+",$query);
  $data  = get_data($query);
}

fwrite($myfile, $doc->saveXML() );
fclose($q_file);
fclose($myfile);





#------------ FUNCTION ----------------------------------------

function get_data($query)
{
    $hostname = "http://www.degruyter.com";
    $request = "/search?f_0=author&q_0=$query&searchTitles=false&type=journals&type_0=journals";
    
    $data = get_html($hostname . $request);

    $doc = new DOMDocument();
    $doc->loadHTML($data);

    $xpath = new DOMXpath($doc);

    $link_to_full_item = $xpath->query('//h2[@class="itemTitle"]/a');
    for ($i=0; $i < $link_to_full_item->length ; $i++)
    {
        $data = get_html( $hostname . $link_to_full_item->item($i)->getAttribute('href') );
        $details = fetch_full_item($data);
        AddJournal_to_XML($details,$GLOBALS['myfile'],$GLOBALS['doc']);
    }
    return "";
}

function fetch_full_item($data)
{
    # Fetch full item
    $details = array();
    $doc = new DOMDocument();
    $doc->loadHTML($data);

    $xpath = new DOMXpath($doc);

    $details['journal_title'] = $xpath->query('//h1[@id="mainTitle"]')->item(0)->textContent;
    $details['article_title'] = $xpath->query('//h1[@class="entryTitle"]')->item(0)->textContent;
    $authors                  = $xpath->query('//div[@class="contributors"]')->item(0)->textContent;
    $details['authors']       = str_replace(" / "," | ",$authors);
    $details['doi']           = "doi:" . $xpath->query('//a[@itemprop="citation_doi"]')->item(0)->textContent;
    
    $keywords = "";
    $dom_keywords = $xpath->query('//p[@class="articleBody_keywords"]/a');
    
    for ($i=0; $i < $dom_keywords->length ; $i++) 
    { 
        if( $keywords == "" )
        {
            $keywords .= $dom_keywords->item($i)->textContent;
        }
        else
        {
            $keywords .= " | " . $dom_keywords->item($i)->textContent;
        }
    }
    $details['keywords'] = $keywords;
    $details['abstract'] = $xpath->query('//div[@class="articleBody_abstract"]/p')->item(0)->textContent;
    $citation_info = $xpath->query('//div[@id="citationInfo"]/p/span')->item(2)->textContent;
    $arr_info = explode(", ", $citation_info);
    

    foreach ($arr_info as $info)
    {
        $tmp = explode(" ",$info);
        if( count($tmp) == 2 )
        {
            $details[$tmp[0]] = $tmp[1];
        }
        elseif( count($tmp) == 3 )
        {
            $key = $tmp[0] . "_" .  $tmp[1];
            $key = str_replace("(","",$key);
            $key = str_replace(")","",$key);
            $details[$key] = $tmp[2];
        }
    }
    
    /*foreach ($details as $key => $value) 
    {
        print "$key :: $value\n";
    }*/
    return $details;
}

function get_html($request)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $request);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

function AddJournal_to_XML($hash,$myfile,$doc)
{
    $root = $doc->createElement('record');
    $root = $doc->appendChild($root);

    
    foreach ($hash as $key => $value)
    {
        $title = $doc->createElement($key);
        $title = $root->appendChild($title);

        $text = $doc->createTextNode($value);
        $text = $title->appendChild($text);
    }
}

function get_param($argv)
{
    $query_filename = "";
    $output_filename = "";
    for ($i=0; $i < sizeof($argv); $i++) 
    { 
        switch ($argv[$i]) 
        {
            case "-h":help();break;
            case "-f":$query_filename = $argv[$i+1];break;
            case "-o":$output_filename = $argv[$i+1];break;
            default:break;
        }  
    }

    # Set defualt output
    if( $output_filename == "" ) $output_filename = "/tmp/output.xml";

    return array('qf' => $query_filename, 'of' => $output_filename);
}

function help()
{
    print "Usage php test.php -f path/to/file [-o /path/to/output.xml] \n\n";
    die;
}

?>